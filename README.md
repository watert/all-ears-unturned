# All Ears Unturned
All Ears Unturned는 업적 '경청하는 자세`All Ears`'와 '모든 돌을 뒤집다`No Stone Unturned`'의 달성을 돕는 가이드 오버레이입니다.

이 오버레이는 '경청하는 자세'를 달성하기 위해 필요한 과정을 순서대로 제공합니다. 지역과 NPC에 대한 기본적인 지식이 필요합니다.  
'모든 돌을 뒤집다'를 달성하기 위해 필요한 주변 지식의 목록도 확인하실 수 있습니다.

## 사용 방법
1. 페이지에서 [Releases](https://gitlab.com/watert/all-ears-unturned/-/releases) 섹션을 찾습니다.
2. AllEarsUnturned.zip 파일을 내려받습니다.
3. 원하는 곳에서 압축을 해제합니다.
4. exe파일을 실행하여 사용 방법을 확인합니다.

오버레이를 게임 화면 위로 표시하려면 전체 화면 모드가 아니어야 합니다.

## 시스템 요구사항
OpenGL 3.0

## Special Thanks
[원본](https://www.reddit.com/r/pathofexile/comments/smtf2u/all_ears_unturned_130_updated_for_317) 프로젝트와 [전체 리스트](https://docs.google.com/spreadsheets/d/1RWPN_NO3wcl7QRTHP6iTONG-T47VForpQgZ-eN2IQzc)의 작성자 두 분 [Nickswoboda](https://github.com/Nickswoboda), [MysticalChill](https://twitch.tv/mysticalchill)과 Path of Exile의 [공식 위키](https://www.poewiki.net/), [PoEDB](https://poedb.tw/kr)의 관계자 모두에게 감사의 인사를 전합니다.
