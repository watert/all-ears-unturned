#include "FileDialog.h"

#include <imgui.h>

#include <filesystem>
#include <iostream>
#include <set>

#include <Windows.h>

FileDialog::FileDialog(int width)
	: width_(width)
{
	//get list of physical drives
	constexpr DWORD bufferlength = 100;
	char drive_buffer[bufferlength];
	DWORD string_length = GetLogicalDriveStrings(bufferlength, drive_buffer);
	if (string_length > 0 && string_length <= 100) {
		char* drive = drive_buffer;
		while (*drive) {
			drives_.push_back(drive);
			drive += strlen(drive_buffer) + 1;
		}
	}
}

void FileDialog::Render()
{
	ImGui::TextWrapped(u8"Path of Exile 게임 폴더 내부의 로그 파일(logs/KakaoClient.txt)을 선택해주세요");
	ImGui::TextWrapped(current_file_path_.c_str());

	std::filesystem::path path = current_file_path_;

	if (prev_paths_.empty()) {
		ImGui::BeginChildFrame(1, { width_ - 5.0f, 300 });
		int index = 0;
		for (const auto& drive : drives_) {
			if (ImGui::Selectable(drive.c_str(), index_selected_ == index, ImGuiSelectableFlags_AllowDoubleClick)) {

				if (ImGui::IsMouseDoubleClicked(0)) {
					prev_paths_.push(drive);
					current_file_path_ = drive;
				}

				index_selected_ = index;
				selected_path_ = drive;
			}
			++index;
		}
		ImGui::EndChildFrame();
	}
	else {
		//try/catch needed for when choosing empty media device such as dvd player
		try {
			if (!std::filesystem::exists(path)) {
				std::cout << "file path does not exist";
				return;
			}

			ImGui::BeginChildFrame(1, { width_ - 5.0f, 300 });

			int index = 0;
			std::set<std::filesystem::path> dirs;
			std::set<std::filesystem::path> files;

			for (auto& p : std::filesystem::directory_iterator(path, std::filesystem::directory_options::skip_permission_denied)) {
				if (p.is_directory()) { dirs.insert(p.path()); }
				else { files.insert(p.path()); }
			}
			for (auto& p : dirs) {
				if (ImGui::Selectable(p.filename().u8string().c_str(), index_selected_ == index, ImGuiSelectableFlags_AllowDoubleClick)) {
					index_selected_ = index;
					selected_path_ = p;

					if (ImGui::IsMouseDoubleClicked(0)) {
						prev_paths_.push(current_file_path_);
						current_file_path_ = p.u8string();
						selected_path_.clear();
					}
				}

				++index;
			}
			ImGui::Separator();
			for (auto&p : files) {
				if (ImGui::Selectable(p.filename().u8string().c_str(), index_selected_ == index, ImGuiSelectableFlags_AllowDoubleClick)) {
					index_selected_ = index;
					selected_path_ = p;

					if (ImGui::IsMouseDoubleClicked(0)) {
						prev_paths_.push(current_file_path_);
						current_file_path_ = p.u8string();
						log_file_path_ = current_file_path_;
						done_ = true;
					}
				}

				++index;
			}

			ImGui::EndChildFrame();

			ImGui::TextWrapped(u8"선택된 파일:");
			ImGui::SameLine();
			ImGui::TextWrapped(selected_path_.filename().u8string().c_str());
		}
		catch (std::filesystem::filesystem_error & error) {};
	}

	if (ImGui::Button("Prev")) {
		if (!prev_paths_.empty()) {
			current_file_path_ = prev_paths_.top();
			prev_paths_.pop();
		}
	}
	ImGui::SameLine();
	if (ImGui::Button("Select")) {
		if (!selected_path_.empty()) {
			if (std::filesystem::is_directory(selected_path_)) {
				prev_paths_.push(current_file_path_);
				current_file_path_ = selected_path_.u8string();
				selected_path_.clear();
			}
			else {
				log_file_path_ = selected_path_.u8string();
				done_ = true;
			}
		}
	}
}

