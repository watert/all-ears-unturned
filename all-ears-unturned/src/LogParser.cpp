#include "LogParser.h"

#include <fstream>
#include <sstream>

void LogParser::Init()
{
	std::ifstream file(log_file_path_, std::ios::in | std::ios::binary);
	if (file) {
		file.seekg(0, std::ios::end);
		end_of_log_ = file.tellg();
		file.close();
	}
	else {
		std::cout << "Could not open log file.";
	}
}

std::string LogParser::GetLocation()
{
	std::ifstream file(log_file_path_, std::ios::in);

	file.seekg(end_of_log_);
	std::stringstream buffer;
	buffer << file.rdbuf();

	file.seekg(0, std::ios::end);
	end_of_log_ = file.tellg();

	file.close();

	std::string log_text = buffer.str();

	// to korean word-order
	auto pos = log_text.find(u8"에 진입했습니다.");
	if (pos != std::string::npos) {
		std::string zone = log_text.substr(0, pos);
		auto fpos = zone.find(" : ");
		zone.erase(0, fpos + 3);

		return zone;
	}
	// original word-order
	pos = log_text.find("You have entered");
	if (pos != std::string::npos) {
		std::string zone;

		for (int i = pos + 17; i < log_text.length() - 2; i++) {
			zone += log_text[i];
		}
		return zone;
	}

	return "";
}

