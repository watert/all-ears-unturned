#include "Application.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_glfw.h>

#include <json.hpp>

#include <iostream>
#include <iomanip>
#include <fstream>

Application::Application(int width, int height)
	:window_(width, height)
{
	if (!gladLoadGLLoader(GLADloadproc(glfwGetProcAddress))) {
		std::cout << "could not load GLAD";
	}

	PushState(State::GUIDE);
	Load();
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	if (std::filesystem::exists("assets/fonts/neodgm_pro.ttf")) {
		io.Fonts->AddFontFromFileTTF("assets/fonts/neodgm_pro.ttf", font_size_, NULL, io.Fonts->GetGlyphRangesKorean());
	}

	ImGui_ImplGlfw_InitForOpenGL(window_.glfw_window_, true);
	ImGui_ImplOpenGL3_Init("#version 410");

	SetImGuiStyle();

	if (!AssetsExist()) {
		PushState(State::LOAD_DATA_ERROR);
		error_message_ = "Unable to load one or more asset files.";
	}
	
	glfwSetWindowUserPointer(window_.glfw_window_, this);
	SetKeyCallbacks();
}

Application::~Application()
{
	Save();
	glfwTerminate();
}

void Application::Run()
{
	while (!glfwWindowShouldClose(window_.glfw_window_) && running_)
	{
		static int frames = 0;
		if (frames > 20) {
			frames = 0;
			if (state_stack_.top() == State::GUIDE) {
				Update();
			}
		}
		++frames;

		if (Window::IsFocused()) {
			Render();
		}

		glfwPollEvents();

		//Must change font outside of ImGui Rendering
		if (font_size_changed_) {
			font_size_changed_ = false;

			ImGuiIO& io = ImGui::GetIO();
			delete io.Fonts;
			io.Fonts = new ImFontAtlas();
			io.Fonts->AddFontFromFileTTF("assets/fonts/neodgm_pro.ttf", font_size_, NULL, io.Fonts->GetGlyphRangesKorean());
			ImGui_ImplOpenGL3_CreateFontsTexture();
		}
	}

}

void Application::Update()
{
	auto location = log_parser_.GetLocation();

	if (all_ears_enabled_ && all_ears_manager_.StepIsComplete(location)) {
		all_ears_manager_.IncrementStep();
	}

	if (!Window::IsFocused()) {
		Render();
	}
}

void Application::Render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	if (window_.collapsed_) {
		ImGui::SetNextWindowSize({ 100.0, (float)window_.height_ });
		ImGui::Begin("All Ears Unturned", &running_, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoSavedSettings);
	}
	else {
		ImGui::SetNextWindowSize({ (float)window_.width_, (float)window_.height_ });
		if (window_.movable_) {
			ImGui::Begin("All Ears Unturned", &running_, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoSavedSettings);
		}
		else {
			ImGui::Begin("All Ears Unturned", &running_, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoMove);
		}

		ImGui::Separator();

		switch (state_stack_.top()) {
			case State::TUTORIAL:
				RenderTutorial();
				break;

			case State::LOAD_DATA_ERROR:
				RenderErrorMessage();
				break;

			case State::FILE_DIALOG:
				file_dialog_->Render();
				if (file_dialog_->done_) {
					log_parser_.log_file_path_ = file_dialog_->log_file_path_;
					log_parser_.Init();
					PopState();
				}
				break;

			case State::GUIDE:
				if (all_ears_enabled_) {
					all_ears_manager_.Render();
					ImGui::Separator();
				}
				if (no_stone_unturned_enabled_) {
					no_stone_manager_.Render();
				}
				break;

			case State::SETTINGS:
				RenderSettingsMenu();
				break;
		}

		ImGui::Separator();
		if (state_stack_.top() != State::SETTINGS && state_stack_.top() != State::LOAD_DATA_ERROR) {
			if (ImGui::Button(u8"환경설정")) {
				//used to avoid being able infinitely stack states
				if (state_stack_.top() == State::FILE_DIALOG || state_stack_.top() == State::TUTORIAL) {
					PopState();
				}
				else {
					PushState(State::SETTINGS);
				}
			}
		}
	}

	ImVec2 pos = ImGui::GetWindowPos();
	if (pos.x != 0.0f || pos.y != 0.0f) {
		ImGui::SetWindowPos({ 0.0f, 0.0f });
		window_.Move(pos.x, pos.y);
	}

	if (window_.height_ != ImGui::GetCursorPosY()) {
		window_.ResizeHeight(ImGui::GetCursorPosY());
	}
	
	ImGui::End();

	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	glfwSwapBuffers(window_.glfw_window_);
}

void Application::RenderSettingsMenu()
{
	ImGui::TextWrapped(u8"글꼴 크기");
	ImGui::PushItemWidth(window_.width_);
	if (ImGui::DragInt("##font", &font_size_, 1.0f, 10, 32)) {
		font_size_changed_ = true;
	}
	ImGui::TextWrapped(u8"창 크기");
	if (ImGui::DragInt("##width", &window_.width_, 1.0f, 100, 1000)) {
		window_.UpdateSize();

		if (file_dialog_ != nullptr) {
			file_dialog_->width_ = window_.width_;
		}
	}
	ImGui::PopItemWidth();


	ImGui::TextWrapped(u8"현재 진행 순서");
	ImGui::PushButtonRepeat(true);
	ImGui::SameLine();
	int step = all_ears_manager_.current_step_ + 1;
	if (ImGui::InputInt("##step", &step)) {
		if (step > 0 && step < all_ears_manager_.steps_.size()) {
			all_ears_manager_.SetCurrentStep(step - 1);
		}
	}
	ImGui::PopButtonRepeat();

	ImGui::Checkbox(u8"창 이동 가능", &window_.movable_);
	ImGui::Checkbox(u8"경청하는 자세", &all_ears_enabled_);
	ImGui::Checkbox(u8"모든 돌을 뒤집다", &no_stone_unturned_enabled_);
	ImGui::Checkbox(u8"단축키 사용", &hotkeys_enabled_);

	ImGui::TextWrapped(u8"게임 로그 경로:");
	ImGui::TextWrapped(log_parser_.log_file_path_.c_str());
	if (ImGui::Button(u8"경로 찾기")) {
		PushState(State::FILE_DIALOG);
	}

	if (ImGui::Button(u8"사용 방법")) {
		PushState(State::TUTORIAL);
	}
	if (ImGui::Button(u8"저장")) {
		Save();
	}


	if (ImGui::Button(u8"돌아가기")) {
		PopState();
	}

}
void Application::RenderTutorial()
{
	ImGui::TextWrapped(u8"사용 방법");
	ImGui::Separator();

	int num_pages = 4;
	switch (tutorial_page_) {
		case 1: 
			ImGui::Bullet();
			ImGui::TextWrapped(u8"경청하는 자세의 진행 순서를 정확히 따라주세요. 새로운 장소에 진입하거나 퀘스트를 완료하지 않아야 합니다. 특정 대화문을 놓치게 될 수 있어요.");
			ImGui::Bullet();
			ImGui::TextWrapped(u8"몇몇 대화문은 퀘스트 보상을 받아야 들을 수 있습니다. 퀘스트 중 주어진 보상을 더 진행하기 전에 받아주세요.");
			ImGui::Bullet();
			ImGui::TextWrapped(u8"주의: 업적을 달성하려면 모든 도적 우두머리들을 죽여야 합니다.");
			break;
		case 2:
			ImGui::Bullet();
			ImGui::TextWrapped(u8"주어진 목표를 순서대로 완수하고 체크박스에 표시합니다. 진행 순서는 모든 목표가 표시되면 자동으로 넘어갑니다.");
			ImGui::Bullet();
			ImGui::TextWrapped(u8"설정 메뉴에서 게임의 로그 위치를 지정할 수 있습니다. 이 경우, 목표 장소에 진입했을 때 역시 자동으로 진행됩니다.");
			ImGui::Bullet();
			ImGui::TextWrapped(u8"표시할 업적을 정해서 볼 수도 있어요.");
			break;
		case 3:
			ImGui::Bullet();
			ImGui::TextWrapped(u8"모든 장을 클리어하고난 뒤 484개의 대화문을 모두 들어야 합니다. 게임 내 업적 창에서 달성도가 일치하는지 확인할 수 있습니다.");
			ImGui::Bullet();
			ImGui::TextWrapped(u8"수가 모자랄 경우 누락한 대화문이 있다는 뜻으로, 업적 달성을 위해서는 캐릭터를 다시 만들어 각 장을 새로 진행해야할 수 있습니다.");
			ImGui::Bullet();
			ImGui::TextWrapped(u8"모든 절차를 그대로 따랐음에도 문제가 발생했을 경우, 깃랩이나 트게더 댓글, 트위치 귓속말 등을 통해 알려주세요. 빠르게 수정하고 경우에 따라 오류를 원작자에게 전달하겠습니다. 감사합니다.");
			break;
		case 4:
			ImGui::Bullet();
			ImGui::TextWrapped(u8"창 자체가 마우스 클릭을 통과시키지 않는 대신, 단축키 E로 창 크기를 줄이고 늘릴 수 있습니다.");
			ImGui::Bullet();
			ImGui::TextWrapped(u8"단축키 Q와 W로 '경청하는 자세'의, A와 S로 '모든 돌을 뒤집다'의 진행 순서를 좌우로 넘길 수 있습니다.");
			ImGui::Bullet();
			ImGui::TextWrapped(u8"환경설정 메뉴에서 글씨 크기, 창의 가로 폭을 바꿀 수 있어요. 세로 크기는 컨텐츠의 양에 따라 자동으로 조절됩니다.");
			ImGui::Bullet();
			ImGui::TextWrapped(u8"창 이동 가능 체크박스를 해제하면 드래그에 의해 창 위치가 틀어지지 않도록 고정할 수 있어요.");
			ImGui::Bullet();
			ImGui::TextWrapped(u8"이 내용 역시 환경설정 메뉴 내의 사용 방법 버튼을 눌러 언제든 다시 확인할 수 있습니다.");
	}

	if (ImGui::ArrowButton("Left", ImGuiDir_Left) && tutorial_page_ > 1) {
		--tutorial_page_;
	}
	ImGui::SameLine();
	ImGui::Text("%d / %d", tutorial_page_, num_pages);
	ImGui::SameLine();
	if (ImGui::ArrowButton("Right", ImGuiDir_Right) && tutorial_page_ < num_pages) {
		++tutorial_page_;
	}

	if (ImGui::Button(u8"알았어요!")) {
		PopState();
	}

}

void Application::RenderErrorMessage()
{
	ImGui::TextWrapped(error_message_.c_str());

	if (ImGui::Button("OK") && AssetsExist()) {
		PopState();
	}
}

void Application::PushState(State state)
{
	state_stack_.push(state);

	if (state == State::FILE_DIALOG) {
		file_dialog_ = std::make_unique<FileDialog>(window_.width_);
	}
}

void Application::PopState()
{
	if (state_stack_.top() == State::FILE_DIALOG) {
		file_dialog_.release();
	}

	state_stack_.pop();
}

void Application::SetImGuiStyle()
{
	ImGuiStyle* style = &ImGui::GetStyle();

	style->WindowPadding = ImVec2(2, 2);

	style->WindowRounding = 0.0f;
	style->Colors[ImGuiCol_TitleBg] = ImVec4(0.0f, 0.0f, 0.0f, 0.2f);
	style->Colors[ImGuiCol_TitleBgActive] = ImVec4(0.0f, 0.0f, 0.0f, 0.8f);
	style->Colors[ImGuiCol_WindowBg] = ImVec4(0.0f, 0.0f, 0.0f, 0.8f);
}

void Application::Load()
{
	all_ears_manager_.LoadData();
	std::ifstream save_file("assets/save-info.json");
	if (!save_file.is_open()) {
		PushState(State::TUTORIAL);
		no_stone_manager_.LoadData();
		
		return;
	}

	nlohmann::json json = nlohmann::json::parse(save_file);
	save_file.close();

	
	if (json.count("log file path") && json.count("current step") && json.count("window x") &&
		json.count("window y") && json.count("window width") && json.count("font size") && 
		json.count("no stone unturned enabled") && json.count("all ears enabled")) {

		log_parser_.log_file_path_ = (json["log file path"]);
		log_parser_.Init();
		all_ears_manager_.SetCurrentStep(json["current step"]);
		window_.Move(json["window x"], json["window y"]);
		window_.width_ = (json["window width"]);
		font_size_ = json["font size"];
		all_ears_enabled_ = json["all ears enabled"];
		no_stone_unturned_enabled_ = json["no stone unturned enabled"];
		no_stone_manager_.LoadData(json);
	}
	else {
		PushState(State::LOAD_DATA_ERROR);
		error_message_ = "No save data was found. Settings will return to their default values.";
	}
}

bool Application::AssetsExist() const
{
	return std::filesystem::exists("assets/fonts/neodgm_pro.ttf") && std::filesystem::exists("assets/steps.json") && std::filesystem::exists("assets/no-stone-unturned.json");
}

void Application::Save()
{
	nlohmann::json json;
	json["log file path"] = log_parser_.log_file_path_;
	json["current step"] = all_ears_manager_.current_step_;
	json["window x"] = window_.x_pos_;
	json["window y"] = window_.y_pos_;
	json["window width"] = window_.width_;
	json["font size"] = font_size_;
	json["all ears enabled"] = all_ears_enabled_;
	json["no stone unturned enabled"] = no_stone_unturned_enabled_;

	no_stone_manager_.Save(json);

	std::ofstream file("assets/save-info.json");
	file << std::setw(4) << json << std::endl;
	file.close();
}

void Application::SetKeyCallbacks()
{
	glfwSetKeyCallback(window_.glfw_window_, [](GLFWwindow* window, int key, int scancode, int action, int mods) {

		auto* app = (Application*)glfwGetWindowUserPointer(Window::glfw_window_);
		if (key == GLFW_KEY_E && action == GLFW_PRESS) {
			if (!app->window_.collapsed_) {
				app->window_.collapsed_ = true;
			}
			else {
				app->window_.collapsed_ = false;
			}
		}
		if (!app->hotkeys_enabled_) {
			return;
		}
		if (app->all_ears_enabled_) {
			if (key == GLFW_KEY_Q && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
				app->all_ears_manager_.DecrementStep();
			}
			if (key == GLFW_KEY_W && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
				app->all_ears_manager_.IncrementStep();
			}
		}
		if (app->no_stone_unturned_enabled_) {
			if (key == GLFW_KEY_A && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
				app->no_stone_manager_.Decrement();
			}
			if (key == GLFW_KEY_S && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
				app->no_stone_manager_.Increment();
			}
		}
	});
}


